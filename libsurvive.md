---
title: "Monado - Using libsurvive"
layout: main
---

# The libsurvive driver in Monado

Before enabling the libsurvive driver in Monado, libsurvive has to be installed as a library before compiling monado. You can follow the build instructions from the [README.md](https://github.com/cntools/libsurvive), make sure to install it as well.

To check whether Monado will be built with libsurvive look at the output from CMake and look for:

```txt
-- #    DRIVER_SURVIVE:      ON
```
You can also do a runtime check if Monado was built with libsurvive support, run `monado-cli probe` and check whether it appears in the list of `Built in drivers`.

When starting any libsurvive or OpenXR application, libsurvive will use cached calibration in `$XDG_CONFIG_HOME/libsurvive/config.json`, or `~/.config/libsurvive/config.json` or run a fresh calibration if this config does not exist yet.

The device the calibration is performed on should not be moved during calibration. Calibration and running with libsurvive may work best when as many device sensors as possible can "see" both basestations at the same time, and when the distance to the basestations is at least 0.5-1 meter.

Note that monado does not pass parameters to libsurvive. For example to force calibration with a specific device use the parameter `--requiredtrackersforcal` with any of the libsurvive tools.

An example to calibrate using a Valve Index HMD, would be `survive-cli --requiredtrackersforcal T20 --allowalltrackersforcal 0`.

To remove libsurvive's calibration data (e.g. to force recalibration or after a significant libsurvive update) delete the configuration file:

    rm ~/.config/libsurvive/config.json

Initial calibration of libsurvive will consider the HMD's location the floor and one of the basestations the forward direction of the space. If calibration was first performed above floor delete the configuration file and begin a new calibration on the floor with the headset facing the base station you would deem 'forward'.

If poor tracking quality persists please attempt rearranging base stations trial and error. Certain physical placements of lighthouses will produce near universally poor results. Keep at least one mobile in order to prevent a poor calibration until global annealing is implemented.

Libsurvive's global solver can be frozen in a known good state such that the lighthouses are not allowed to continuously recalibrate during the tracking session with the enviornment variable `SURVIVE_GLOBALSCENESOLVER=0`.

This will retain a static configuration should the user happen to attempt to manually eliminate or reduce the local minima to a desirable state and halt all further calibration. This can help reduce floating and sliding of the pose in certain pockets of space.

## Tracking Quality:

Without a more reliable lighthouse pose calibration, libsurvive is imperfect for users coming from SteamVR. Thankfully, you can import your steamvr calibration using the `survive-cli --steamvr-calibration` flag.

Here is how:

* Ensure you have launched steamvr at least once on your PC, and delete your `~/.config/libsurvive/config.json` if it exists.
* By default, libsurvive will seek out `~/.steam/steam/config/lighthouse/lighthousedb.json` but you may provide the location of a file after the `--steamvr-calibration $FILE`. (So far, all `lighthousedb.json` files valid for your current lighthouse arrangement, produced by Windows or Linux SteamVR, have been tested to work.)
* Wait for libsurvive to get the OOTX packets and preambles and produce one `Info: MPFIT success <...>` message. At this point, libsurvive will take your `lighthousedb.json` and produce a `~/.config/libsurvive/config.json` using the exact parameters of your SteamVR calibration.
* Run all future Monado/libsurvive sessions with `SURVIVE_GLOBALSCENESOLVER=0` to prevent the survive driver from changing your libsurvive config. It may be handy to add this value to your `/etc/environment` file for convenience.
* Try not to move your lighthouses from this point onward - their position won't be recalibrated if you have set `SURVIVE_GLOBALSCENESOLVER=0`, and tracking won't work. If you do move them, rerun SteamVR to get a new `lighthousedb.json` then repeat this process.
* If you require a solid config with no drift in a wide area, this is the best solution.

