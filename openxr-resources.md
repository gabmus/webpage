---
title: "Monado - List of OpenXR resources"
layout: main
---

# Engine Integration

* [https://github.com/EpicGames/UnrealEngine/tree/release/Engine/Plugins/Runtime/OpenXR](https://github.com/EpicGames/UnrealEngine/tree/release/Engine/Plugins/Runtime/OpenXR)
  * Platform: Windows
  * Graphics API: OpenGL, Vulkan, D3D
  * Language: C++
* [https://gitlab.freedesktop.org/monado/demos/godot_openxr/](https://gitlab.freedesktop.org/monado/demos/godot_openxr/)
  * Platform: Linux
  * Graphics API: OpenGL (ES, but uses Desktop GL bindings)
  * Language: C

# Code Examples and References

* [https://github.com/KhronosGroup/OpenXR-SDK-Source/tree/master/src/tests/hello_xr](https://github.com/KhronosGroup/OpenXR-SDK-Source/tree/master/src/tests/hello_xr)
  * Platform: Linux, Windows
  * Graphics API: OpenGL, Vulkan, D3D
  * Language: C++
* [https://gitlab.freedesktop.org/monado/demos/xrgears](https://gitlab.freedesktop.org/monado/demos/xrgears)
  * Platform: Linux
  * Graphics API: Vulkan
  * Language: C++
* [https://gitlab.freedesktop.org/monado/demos/openxr-simple-example](https://gitlab.freedesktop.org/monado/demos/openxr-simple-example)
  * Platform: Linux
  * Graphics API: OpenGL
  * Language: C
* [https://github.com/jherico/OpenXR-Samples/](https://github.com/jherico/OpenXR-Samples/)
  * Platform: Windows
  * Graphics API: OpenGL
  * Language: C++
* [https://gitlab.freedesktop.org/xrdesktop/gxr](https://gitlab.freedesktop.org/xrdesktop/gxr)
  * Platform: Linux
  * Graphics API: Vulkan with OpenGL through external memory/memory objects
  * Language: C with glib
* [https://github.com/microsoft/OpenXR-MixedReality](https://github.com/microsoft/OpenXR-MixedReality)
  * Platform: Windows
  * Graphics API: D3D
  * Language: C++
* [https://github.com/maluoi/OpenXRSamples](https://github.com/maluoi/OpenXRSamples) / [https://playdeck.net/blog/introduction-to-openxr](https://playdeck.net/blog/introduction-to-openxr)
  * Platform: Windows
  * Graphics API: D3D
  * Language: C++
* [https://github.com/maluoi/StereoKit](https://github.com/maluoi/StereoKit)
  * Platform: Windows (independent linux port in progress)
  * Graphics API: D3D, OpenGL
  * Language: C#

# Games

* [The Dark Mod VR](https://github.com/fholger/thedarkmodvr)
  * Platform: Windows, Linux
  * Graphics API: OpenGL
  * Language: C++
* [OpenMW (fork)](https://gitlab.com/madsbuvi/openmw/) (Requires commercial assets)
  * Platform: Windows, Linux
  * Graphics API: OpenGL
  * Language: C++

# Other

* [https://crates.io/crates/openxr/](https://crates.io/crates/openxr/)
  * Rust bindings
* [https://devtalk.blender.org/t/gsoc-2019-vr-support-through-openxr-weekly-reports/7665](https://devtalk.blender.org/t/gsoc-2019-vr-support-through-openxr-weekly-reports/7665)
  * Blender OpenXR "Scene Inspection" GSOC progress reports with links to implementation commits
* [quick3d-openxr](https://github.com/technobaboo/quick3d-openxr)
  * OpenXR in a Qt QML component
* [FreeCAD (fork)](https://github.com/kwahoo2/FreeCAD)
  * See [this forum thread for information](https://forum.freecadweb.org/viewtopic.php?f=8&t=39526)
